from odoo import models

class BahanXlsx(models.AbstractModel):
    _name = 'report.hmcoffee.report_bahan_xlsx'
    _inherit= 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, bahan):
        sheet = workbook.add_worksheet('Daftar Menu')
        row = 0
        col = 0
        sheet.write(row,col,'Nama Menu:')
        sheet.write(row,col+1,'Jumlah Stok:')
        sheet.write(row,col+2,'Harga Modal:')
        sheet.write(row,col+3,'Total Modal:')
        sheet.write(row,col+4,'Kondisi Stok:')
        for obj in bahan:
            row += 1

        # Menulis data bahan
        row = 1
        for obj in bahan:
            col = 0
            sheet.write(row, col, obj.nama_bahan)
            sheet.write(row, col + 1, obj.stok)
            sheet.write(row, col + 2, obj.harga_modal)
            sheet.write(row, col + 3, obj.total_modal)
            sheet.write(row, col + 4, obj.kondisi_stok)
            row += 1
    