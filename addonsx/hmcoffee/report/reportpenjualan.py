from odoo import models

class PenjualanXlsx(models.AbstractModel):
    _name = 'report.hmcoffee.report_penjualan_xlsx'
    _inherit= 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, penjualan):
        sheet = workbook.add_worksheet('Daftar Penjualan')
        row = 0
        col = 0
        sheet.write(row,col,'Nama Pembeli:')
        sheet.write(row,col+1,'Tanggal Transaksi:')
        sheet.write(row,col+2,'Total Harga:')
        for obj in penjualan:
            row += 1

        # Menulis data bahan
        row = 1
        for obj in penjualan:
            col = 0
            sheet.write(row, col, obj.name)
            sheet.write(row, col + 1, obj.tgl_transaksi)
            sheet.write(row, col + 2, obj.total_harga)
            row += 1