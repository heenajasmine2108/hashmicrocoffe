from odoo import models, fields, api

class Product(models.Model):
    _inherit = 'product.template'

    supplier = fields.Char(string='Supplier')